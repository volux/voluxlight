# Volux Light <!-- omit in toc -->

[![PyPI](https://img.shields.io/pypi/v/voluxlight?logo=python)](https://pypi.org/project/voluxlight)
[![PyPI - Downloads](https://img.shields.io/pypi/dm/voluxlight?logo=Python)](https://pypi.org/project/voluxlight)
[![PyPI - License](https://img.shields.io/pypi/l/voluxlight?color=orange&logo=Python)](https://pypi.org/project/voluxlight)

---

☠️ **EXPERIMENTAL:** - Beware all ye who enter here ☠️

---

## Table of Contents <!-- omit in toc -->

- [Installation](#installation)
- [Usage](#usage)
  - [`VoluxLightLifxLan` - Destination](#voluxlightlifxlan---destination)
    - [Examples](#examples)
- [To-do List](#to-do-list)
- [Links](#links)


## Installation 

```bash
pip install voluxlight
```

## Usage

### `VoluxLightLifxLan` - Destination

#### Examples

- [Hue Demo](https://gitlab.com/volux/volux/-/blob/master/voluxcli/voluxcli/demos/hue_demo.py) (available in the [main Volux repository](https://gitlab.com/volux/volux/))

<!-- TODO: add more example/s -->

## To-do List

<!-- TODO: add todo list (the irony) -->

- [x] Get `VoluxLightLifxLan` class working
- [x] Add basic example to documentation
- [ ] Replace link to basic example with a new example in a code block
- [ ] Add basic documentation
- [ ] Add more examples to documentation
- [ ] Add more detailed documentation

## Links

<!-- TODO: add website link -->
- 📖 &nbsp;[Documentation](https://gitlab.com/volux/voluxlight)
- 🐍 &nbsp;[Latest Release](https://pypi.org/project/voluxlight)
- 🧰 &nbsp;[Source Code](https://gitlab.com/volux/voluxlight)
- 🐞 &nbsp;[Issue Tracker](https://gitlab.com/volux/voluxlight/-/issues)
- `🐦 Twitter` &nbsp;[@DrTexx](https://twitter.com/DrTexx)
- `📨 Email` &nbsp;[denver.opensource@tutanota.com](mailto:denver.opensource@tutanota.com)
